<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;

class ContactController extends FOSRestController implements ClassResourceInterface
{
	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function getAction($id)
	{
		$contact = $this->getContactManager()->get($id);

		if (false === $contact) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('get', $contact)) {
          	throw new AccessDeniedException();
        }

		return $contact;
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function cgetAction(Request $request)
	{
		$filters = array('ids' => $request->get('ids',null));
		return $this->getContactManager()->cget($this->getUser(), $filters);
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function postAction(Request $request)
	{
		$contact = new Contact();

		if (false === $this->get('security.authorization_checker')->isGranted('post', $contact)) {
          	throw new AccessDeniedException();
        }

		$form = $this->createForm(new ContactType(), $contact);
		$form->handleRequest($request);
		
		if ($form->isValid()){
			$contact->setOwner($this->getUser());
			$this->getContactManager()->post($contact);
			return $contact;
		} else {
			return $form;
		}
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function patchAction(Request $request, $id)
	{
		$contact = $this->getContactManager()->get($id);

		if (false === $contact) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('patch', $contact)) {
          	throw new AccessDeniedException();
        }

		$form = $this->createForm(new ContactType(), $contact, array('method' => 'PATCH'));
		$form->handleRequest($request);
		
		if ($form->isValid()){
			$this->getContactManager()->patch($contact);
			return $contact;
		} else {
			return $form;
		}
	}

	/**
	 * @Annotations\View()
	 */
	public function deleteAction($id)
	{
		$contact = $this->getContactManager()->get($id);

		if (false === $contact) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('delete', $contact)) {
          	throw new AccessDeniedException();
        }

        $this->getContactManager()->remove($contact);
        
		return true;
	}

	/**
	 * @Annotations\View()
	 */
	public function cdeleteAction(Request $request)
	{
		$this->getContactManager()->cremove($this->getUser(), $request->get('filters'));
		return true;
	}

	public function getContactManager()
	{
		return $this->get('wf.contact_manager');
	}
}