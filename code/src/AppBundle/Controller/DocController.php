<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DocController extends Controller
{
	/**
     * @Route("/terms", name="wf_terms")
     * @Template(":doc:content/terms.html.twig")
     */
    public function termsAction()
    {
        return array();
    }

    /**
     * @Route("/cookies", name="wf_cookies")
     * @Template(":doc:content/cookies.html.twig")
     */
    public function cookiesAction()
    {
        return array();
    }

    /**
     * @Route("/privacy", name="wf_privacy")
     * @Template(":doc:content/privacy.html.twig")
     */
    public function privacyAction()
    {
        return array();
    }
}
