<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use AppBundle\Entity\Notification;
use AppBundle\Form\NotificationType;

class NotificationController extends Controller implements ClassResourceInterface
{
	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function getAction($id)
	{
		$notification = $this->getNotificationManager()->get($id);

		if (false === $notification) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('get', $notification)) {
          	throw new AccessDeniedException();
        }

		return $notification;
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function cgetAction(Request $request)
	{
		$filters = array('ids' => $request->get('ids',null));
		return $this->getNotificationManager()->cget($this->getUser(), $filters);
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function postAction(Request $request)
	{
		$notification = new Contact();

		if (false === $this->get('security.authorization_checker')->isGranted('post', $notification)) {
          	throw new AccessDeniedException();
        }

		$form = $this->createForm(new NotificationType(), $notification);
		$form->handleRequest($request);
		
		if ($form->isValid()){
			$notification->setOwner($this->getUser());
			$this->getNotificationManager()->post($notification);
			return $notification;
		} else {
			return $form;
		}
	}

	/**
	 * @Annotations\View(serializerGroups={"get"})
	 */
	public function patchAction(Request $request, $id)
	{
		$notification = $this->getNotificationManager()->get($id);

		if (false === $notification) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('patch', $notification)) {
          	throw new AccessDeniedException();
        }

		$form = $this->createForm(new NotificationType(), $notification, array('method' => 'PATCH'));
		$form->handleRequest($request);
		
		if ($form->isValid()){
			$this->getNotificationManager()->patch($notification);
			return $notification;
		} else {
			return $form;
		}
	}

	/**
	 * @Annotations\View()
	 */
	public function deleteAction($id)
	{
		$notification = $this->getNotificationManager()->get($id);

		if (false === $notification) {
          	throw new NotFoundHttpException();
        }

		if (false === $this->get('security.authorization_checker')->isGranted('delete', $notification)) {
          	throw new AccessDeniedException();
        }

        $this->getNotificationManager()->remove($notification);
        
		return true;
	}

	/**
	 * @Annotations\View()
	 */
	public function cdeleteAction(Request $request)
	{
		$this->getNotificationManager()->cremove($this->getUser(), $request->get('filters'));
		return true;
	}

	public function getNotificationManager()
	{
		return $this->get('wf.notification_manager');
	}
}
