<?php

namespace AppBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Contact;

class ContactManager
{
    protected $doctrine;

    const CLASSNAME = 'AppBundle\Entity\Contact';

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function get($id)
    {
        $contact = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        return $contact instanceOf Contact ? $contact : false;
    }

    public function cget($user, $filters)
    {
        $parsedFilters['owner'] = $user->getId();
        if (isset($filters['ids']) && !empty($filters['ids'])){
            $parsedFilters['ids'] = explode('_', $filters['ids']);
        }

        $contacts = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($parsedFilters);
        
        return $contacts;
    }

    public function disableById($id)
    {
        $contact = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        if (!$contact instanceOf Contact) return false;
        return $this->disable($contact);
    }

    public function removeById($id)
    {
        $contact = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        if (!$contact instanceOf Contact) return false;
        return $this->remove($contact);
    }

    public function cdisable($user, $filters)
    {
        $filters['owner'] = $user->getId();
        $contacts = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($filters);
        foreach ($contacts as $contact) {
            $this->disable($contact, false);
        }
        $this->doctrine->getManager()->flush();
        return true;
    }

    public function cremove($user, $filters)
    {
        $parsedFilters['owner'] = $user->getId();
        if (isset($filters['ids']) && !empty($filters['ids'])){
            $parsedFilters['ids'] = explode('_', $filters['ids']);
        }

        $contacts = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($parsedFilters);

        foreach ($contacts as $contact) {
            $this->remove($contact, false);
        }
        $this->doctrine->getManager()->flush();
        return true;
    }

    public function post($contact, $doFlush = true)
    {
        $this->doctrine->getManager()->persist($contact);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function patch($contact, $doFlush = true)
    {
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function disable(Contact $contact, $doFlush = true)
    {
        $contact->setEnabled(false);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function remove(Contact $contact, $doFlush = true)
    {
        $this->doctrine->getManager()->remove($contact);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }
}