<?php

namespace AppBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Notification;;

class NotificationManager
{
    protected $doctrine;

    const CLASSNAME = 'AppBundle\Entity\Notification';

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function get($id)
    {
        $notification = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        return $notification instanceOf Notification ? $notification : false;
    }

    public function cget($user, $filters)
    {
        $parsedFilters['owner'] = $user->getId();
        if (isset($filters['ids']) && !empty($filters['ids'])){
            $parsedFilters['ids'] = explode('_', $filters['ids']);
        }

        $notifications = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($parsedFilters);
        
        return $notifications;
    }

    public function disableById($id)
    {
        $notification = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        if (!$notification instanceOf Notification) return false;
        return $this->disable($notification);
    }

    public function removeById($id)
    {
        $notification = $this->doctrine->getRepository(self::CLASSNAME)->findOneById($id);
        if (!$notification instanceOf Notification) return false;
        return $this->remove($notification);
    }

    public function cdisable($user, $filters)
    {
        $filters['owner'] = $user->getId();
        $notifications = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($filters);
        foreach ($notifications as $notification) {
            $this->disable($notification, false);
        }
        $this->doctrine->getManager()->flush();
        return true;
    }

    public function cremove($user, $filters)
    {
        $parsedFilters['owner'] = $user->getId();
        if (isset($filters['ids']) && !empty($filters['ids'])){
            $parsedFilters['ids'] = explode('_', $filters['ids']);
        }

        $notifications = $this->doctrine->getRepository(self::CLASSNAME)->getAllByFilter($parsedFilters);

        foreach ($notifications as $notification) {
            $this->remove($notification, false);
        }
        $this->doctrine->getManager()->flush();
        return true;
    }

    public function post($notification, $doFlush = true)
    {
        $this->doctrine->getManager()->persist($notification);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function patch($notification, $doFlush = true)
    {
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function disable(Notification $notification, $doFlush = true)
    {
        $notification->setEnabled(false);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }

    public function remove(Notification $notification, $doFlush = true)
    {
        $this->doctrine->getManager()->remove($notification);
        if ($doFlush) {
            $this->doctrine->getManager()->flush();
        }
        return true;
    }
}