<?php

namespace AppBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\Contact;

class ContactVoter implements VoterInterface
{
    const GET = 'get';
    const POST = 'post';
    const PATCH = 'patch';
    const DELETE = 'delete';

    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
            self::GET,
            self::POST,
            self::PATCH,
            self::DELETE,
        ));
    }

    public function supportsClass($class)
    {
        $supportedClass = 'AppBundle\Entity\Contact';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }

    public function vote(TokenInterface $token, $contact, array $attributes)
    {
        if (!$this->supportsClass(get_class($contact))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException('Only one attribute is allowed'); }

        $attribute = $attributes[0];

        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        switch($attribute) {
            case self::GET:
                if ($this->isOwner($user, $contact)){
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::POST:
                if (true){
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::PATCH:
                if ($this->isOwner($user, $contact)){
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
            case self::DELETE:
                if ($this->isOwner($user, $contact)){
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }

    public function isOwner(UserInterface $user, $object){
        return $object->getOwner()->getId() === $user->getId();
    }

    public function isEnabled($object){
        return $object->isEnabled();
    }
}