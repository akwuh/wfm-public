<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Contact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 *
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"get"})
     *
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "contact.name.blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "contact.name.length.min",
     *      maxMessage = "contact.name.length.max"
     * )
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \AppBundle\Entity\User
     *
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $owner;

    /**
     * 
     * @var array
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="dates", type="array")
     */
    // TODO: add validation on field Contacts->dates and notes
    private $dates;

    /**
     * @var array
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="notes", type="array")
     */
    private $notes;

    /**
     * @var array
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="preferences", type="array", nullable=true)
     */
    private $preferences;

    /**
     * @var array
     *
     * @ORM\Column(name="history", type="array")
     */
    private $history;

    /**
     * @var \AppBundle\Entity\Notification
     *
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Notification", mappedBy="contact", fetch="EXTRA_LAZY")
     */
    private $notifications;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dates
     *
     * @param array $dates
     * @return Contact
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return array 
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Set notes
     *
     * @param array $notes
     * @return Contact
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return array 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set history
     *
     * @param array $history
     * @return Contact
     */
    public function setHistory($history)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get history
     *
     * @return array 
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     * @return Contact
     */
    public function setOwner(\AppBundle\Entity\User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add notifications
     *
     * @param \AppBundle\Entity\Notification $notifications
     * @return Contact
     */
    public function addNotification(\AppBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \AppBundle\Entity\Notification $notifications
     */
    public function removeNotification(\AppBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Set preferences
     *
     * @param array $preferences
     * @return Contact
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;

        return $this;
    }

    /**
     * Get preferences
     *
     * @return array 
     */
    public function getPreferences()
    {
        return $this->preferences;
    }
}
