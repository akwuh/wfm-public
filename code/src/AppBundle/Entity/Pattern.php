<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pattern
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PatternRepository")
 */
class Pattern
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="usageCount", type="integer")
     */
    private $usageCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Pattern
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set usageCount
     *
     * @param integer $usageCount
     * @return Pattern
     */
    public function setUsageCount($usageCount)
    {
        $this->usageCount = $usageCount;

        return $this;
    }

    /**
     * Get usageCount
     *
     * @return integer 
     */
    public function getUsageCount()
    {
        return $this->usageCount;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pattern
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
