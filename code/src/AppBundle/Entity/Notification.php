<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"get"})
     *
     */
    private $id;

    /**
     * @var string
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var \AppBundle\Entity\User
     *
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $owner;

    /**
     * @var \AppBundle\Entity\Contact
     *
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Contact", inversedBy="notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $contact;

    /**
     * @var boolean
     *
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="smsEnabled", type="boolean")
     */
    private $smsEnabled;

    /**
     * @var boolean
     *
     * @Groups({"get"})
     *
     * @ORM\Column(name="emailEnabled", type="boolean")
     */
    private $emailEnabled;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Notification
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set smsEnabled
     *
     * @param boolean $smsEnabled
     * @return Notification
     */
    public function setSmsEnabled($smsEnabled)
    {
        $this->smsEnabled = $smsEnabled;

        return $this;
    }

    /**
     * Is smsEnabled
     *
     * @return boolean 
     */
    public function isSmsEnabled()
    {
        return $this->smsEnabled;
    }

    /**
     * Set emailEnabled
     *
     * @param boolean $emailEnabled
     * @return Notification
     */
    public function setEmailEnabled($emailEnabled)
    {
        $this->emailEnabled = $emailEnabled;

        return $this;
    }

    /**
     * Is emailEnabled
     *
     * @return boolean 
     */
    public function isEmailEnabled()
    {
        return $this->emailEnabled;
    }

    /**
     * Get smsEnabled
     *
     * @return boolean 
     */
    public function getSmsEnabled()
    {
        return $this->smsEnabled;
    }

    /**
     * Get emailEnabled
     *
     * @return boolean 
     */
    public function getEmailEnabled()
    {
        return $this->emailEnabled;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     * @return Notification
     */
    public function setOwner(\AppBundle\Entity\User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Contact $contact
     * @return Notification
     */
    public function setContact(\AppBundle\Entity\Contact $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }
}
